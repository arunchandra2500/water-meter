# WATER METER


A standard water meter can measure water consumption, determine a leak and measure the amount of water any water source produces.A smart water meter not only measures water flow but uses wireless communication to connect to local or wide area networks, allowing remote location monitoring and infrastructure maintenance through leak detection. The choice depends on the flow measurement method, the type of end user, the required flow rates, and accuracy requirements. Remote monitoring of a system can be enabled by using GPRS, ZIG-BEE, Bluetooth, LoRaWAN etc.The main features of the water meter are to determine the existence of a leak if no water is used, but the meter still records, then there's a leak somewhere and to submeter separate units in a homeowners association or apartment building, so residents can pay for their own water use.

LoRaWAN specification is a Low Power, Wide Area (LPWA) networking protocol designed to wirelessly connect battery operated `things' to the internet in regional, national or global networks, and targets key Internet of Things (IoT) requirements such as bi-directional communication, end-to-end security, mobility and localization services.

LoRaWAN based water meter is used to monitor the total consumption of water remotely. Here we are using a mechanical water meter with pulse output. The meter produce pulses in each 5 litres of water. The firmware of the controller count this pulses and  perform the calculations.


## Getting Started


- Make sure that you have a STM32 based Board.

- Install LoRaWAN software expansion code from [here](https://www.st.com/en/embedded-software/i-cube-lrwan.html) . Copy the code to ~/STM32CubeIDE/workspace/

- Select board : B-L072Z-LRWAN1 Development Board

## Prerequisites

STM32CubeIDE 1.1.0[Tested]

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/arunchandra2500/water-meter/-/blob/master/LICENCE.md) file for details

## Acknowledgments

- LoRaWAN software expansion code of [Semtech](https://www.st.com/en/embedded-software/i-cube-lrwan.html) and Stackforce
